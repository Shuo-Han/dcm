#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import time
import argparse
import shutil
from collections import defaultdict

des = 'sort the dicoms just coppied to iacl'
parser = argparse.ArgumentParser(description=des)
parser.add_argument('-t', '--time', help='find dicom within this time, in days',
                    required=False, default=1, type=float)
parser.add_argument('-o', '--output', help='the output dirname',
                    required=False, default='./converted_scan')
args = parser.parse_args()

def calc_days_from_seconds(secs):
    return float(secs) / 60.0 / 60.0 / 24.0

current_time = time.time()
dirname = '/home/dicom/unsorted'
filenames = os.listdir(dirname)

dcm_filenames = list()
for fn in filenames:
    if fn.endswith('dcm'):
        path = os.path.join(dirname, fn)
        create_time = os.path.getctime(path)
        duration = calc_days_from_seconds(current_time - create_time)
        if duration < args.time:
            dcm_filenames.append(path)

scans = defaultdict(list)
for dfn in dcm_filenames:
    id = '.'.join(dfn.split('.')[10:13])
    scans[id].append(dfn)

for k, v in scans.items():
    scan_dir = os.path.join(dirname, k, 'dicom')
    if not os.path.isdir(scan_dir):
        os.makedirs(scan_dir)
    for vv in v:
        shutil.move(vv, scan_dir)
        print('moving', vv, 'to', scan_dir)
