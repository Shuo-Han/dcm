Sort Dicoms Copied From JHU PACS and Convert Them to .nii Format
--
## Installation

In terminal:

    $ git clone git@bitbucket.org:Shuo-Han/dcm.git

## Usage

Sort the dicom files into separate folder; each folder contains a single scan. Check the help doc:

    $ ./sort_dcm.py -h

Convert the dicoms sorted by the previous command:

    $ ./convert_dcm.py /path/to/the/folder
