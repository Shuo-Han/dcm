#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

dirname = sys.argv[1]
dicom_dirname = os.path.join(dirname, 'dicom')
log_filename = os.path.join(dirname, 'log')

command = 'dcm2niix -f %n-%p-%t-%s -o ' + dirname\
        + ' ' + dicom_dirname + ' > ' + log_filename
print(command)
os.system(command)
