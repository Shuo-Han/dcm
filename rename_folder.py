#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

dirname = sys.argv[1]
files = os.listdir(dirname)

for f in files:
    if f.endswith('nii'):
        ref_name = f
        break

prefix = ref_name.split('-')[0]
tmp = os.path.split(dirname)
tmp[-1] = prefix
new_dirname = os.sep.join(tmp)
print(new_dirname)
